package api;

import java.io.Serializable;

/**
 * Created by krosullivan on 06/02/2017.
 */
public class DataResponse implements Serializable {
    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public DataResponse(Object data) {
        this.data = data;
    }
}
