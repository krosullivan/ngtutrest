package api;

import java.io.Serializable;

public class ToDo  implements Serializable {
    private long id;
    private String description;
    private long priority;

    public long getId() { return id; }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public long getPriority() {
        return priority;
    }

    public void setPriority(long priority) {
        this.priority = priority;
    }

    public ToDo(long id, String description, long priority) {
        this.id = id;
        this.description = description;
        this.priority = priority;
    }

    public ToDo() {}
}
