import {Injectable} from '@angular/core';
import {Http, Response, RequestOptionsArgs} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class RESTBase {
    private baseUrl = API_URL;
    public path: String;

    constructor(public http: Http) {
    }

    create(model: Object): Observable<RESTSuccess> {
        return this.http.post(this.getUrl(), model)
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    find<T>(options?: RequestOptionsArgs): Observable<T[]> {
        return this.http.get(this.getUrl(), options)
            .map((res: Response) => res.json().data)
            .catch((error: any) => Observable.throw(error));
    }

    findOne<T>(id: any): Observable<T> {
        return this.http.get(this.getUrl())
            .map((res: Response) => res.json().data)
            .catch((error: any) => Observable.throw(error));
    }

    update(id: any, model: Object): Observable<RESTSuccess> {
        return this.http.post(this.getUrl(id), model)
            .map((res: Response) => res.json())
            .catch((error: any) => {
                return Observable.throw(error);
            });
    }

    delete(id: any): Observable<RESTSuccess> {
        return this.http.delete(this.getUrl(id))
            .map((res: Response) => res.json())
            .catch((error: any) => Observable.throw(error));
    }

    private getUrl(id?: any): string {
        if (!id) {
            return `${this.baseUrl}/${this.path}`;
        }
        return `${this.baseUrl}/${this.path}/${id}`;
    }
}

interface RESTSuccess {
    message: string;
    creationId?: number;
}