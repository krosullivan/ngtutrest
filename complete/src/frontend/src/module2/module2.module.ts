import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {Module2Component} from './components/module2.component';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: 'module2',
                component: Module2Component
            }
        ])

    ],
    declarations: [Module2Component]
})
export class Module2Module {
}
