import {Component} from '@angular/core';
@Component({
    selector: 'jj-module2',
    templateUrl: './module2.component.html',
    styleUrls: ['./module2.component.scss']
})
export class Module2Component {
    name = 'Module 2';
}
