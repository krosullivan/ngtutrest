import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {ToDoComponent} from './components/todos.component';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: 'todos',
                component: ToDoComponent
            }
        ])
    ],
    declarations: [ToDoComponent]
})
export class ToDosModule {
}
