import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {RESTBase} from '../../common/services/rest-base.service';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class ToDoHTTPService extends RESTBase {
    constructor(http: Http) {
        super(http);
        this.path = 'todos';
    }
}