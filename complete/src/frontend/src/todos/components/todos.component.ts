import {Component, OnInit} from '@angular/core';
import {ToDoHTTPService} from '../services/todos-http.service';
import {ToDo} from '../models/todo.model';

@Component({
    selector: 'jj-todos',
    templateUrl: './todos.component.html',
    styleUrls: ['./todos.component.scss'],
    providers: [ToDoHTTPService]
})
export class ToDoComponent implements OnInit {
    name = 'ToDos';
    todos: ToDo[];

    constructor(private toDoHTTPService: ToDoHTTPService) {
    }

    ngOnInit() {
        this.loadToDos();
        this.createToDo();
        this.updateToDo();
        this.deleteToDo();
    }

    loadToDos() {
        this.toDoHTTPService.find<ToDo>().subscribe(
            todos => this.todos = todos,
            err => console.error(err)
        );
    }

    createToDo() {
        let dummy = new ToDo(100, 'test', 1);
        this.toDoHTTPService.create(dummy).subscribe(
            resp => console.info(resp.message),
            err => console.error(err)
        );
    }

    updateToDo() {
        let dummy = new ToDo(100, 'test', 1);
        this.toDoHTTPService.update(1, dummy).subscribe(
            resp => console.info(resp.message),
            err => console.error(err)
        );
    }

    deleteToDo() {
        let dummy = new ToDo(100, 'test', 1);
        this.toDoHTTPService.delete(1).subscribe(
            resp => console.info(resp.message),
            err => console.error(err)
        );
    }
}
