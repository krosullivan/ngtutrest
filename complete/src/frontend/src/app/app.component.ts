import {Component, ViewEncapsulation} from '@angular/core';
import './styles/main.scss';

@Component({
    selector: 'jj-app',
    templateUrl: './app.component.html',
    styleUrls: ['./styles/main.scss', './app.component.scss'],
    encapsulation: ViewEncapsulation.None
})
export class AppComponent {
}
