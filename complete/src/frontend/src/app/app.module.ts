import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {HttpModule, JsonpModule} from '@angular/http';
import {AppComponent} from './app.component';
import {CommonModule} from '../common/common.module';
import {NavComponent} from '../common/nav/nav.component';
import {MenuComponent} from '../common/menu/menu.component';
import {ToDosModule} from '../todos/todos.module';
import {Module2Module} from '../module2/module2.module';

@NgModule({
    imports: [
        BrowserModule,
        RouterModule.forRoot([
            {
                path: '',
                redirectTo: 'todos',
                pathMatch: 'full'
            }
        ]),
        HttpModule,
        JsonpModule,
        CommonModule,
        ToDosModule,
        Module2Module,
    ],
    declarations: [
        AppComponent,
        NavComponent,
        MenuComponent
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
