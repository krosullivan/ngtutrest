import {TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';
import { RouterTestingModule } from '@angular/router/testing';
import { NavComponent } from '../common/nav/nav.component';
import { MenuComponent } from '../common/menu/menu.component';

describe('App', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [RouterTestingModule],
            declarations: [AppComponent, NavComponent, MenuComponent]
        });
    });
    it('should work', () => {
        let fixture = TestBed.createComponent(AppComponent);
        expect(fixture.componentInstance instanceof AppComponent).toBe(true, 'should create AppComponent');
    });
});
