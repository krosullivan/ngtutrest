# README #

## Development

### 1. Run The Spring Back End###

```
mvm spring-boot:run -Dserver.port=8888
```

### 2. Run Angular Front End ###

This will run the webpack dev server. Api calls will point to `http://localhost:8888/api` being served by spring boot run

```
cd src/frontend && npm start
```

## Tests

### 1. Front End Tests

```
npm test
```

## Packaging

The front end app will be compiled and moved to target/classes/static. Running the jar will result in the front end app available at `http://localhost:8888`. API calls will point to the packaged spring app at `http://localhost:8888/api`

```
mvn clean package
```